class Certificate
  def initialize(args={})
    @name     = args[:name]
    @address  = args[:address]
    @tel      = args[:tel]
  end

  def issue
    output_certificate_name
    output_name
    output_address
    output_tel
    output_sign
  end

  private

  def output_certificate_name
    raise 'Not implemented!'
  end

  def output_name
    puts @name
  end

  def output_address
    puts @address
  end

  def output_tel
    puts @tel
  end

  def output_sign
    raise 'Not implemented!'
  end
end

class Plain < Certificate
  private
  def output_certificate_name
    puts '平文'
  end

  def output_sign
    puts 'オレオレ証明書'
  end
end

class ResidentCard < Certificate
   
  private
  def output_certificate_name
    puts '住民票'
  end

  def output_sign
    puts '区役所より'
  end
end

class DrivingLicence < Certificate
  #!!!!
  def issue
    output_certificate_name
    output_address
    output_tel
    output_name
    output_sign
  end

  private
  def output_certificate_name
    puts '★★★運転免許証★★★'
  end

  def output_address
    printf "住所:#{@address}\t"
  end

  def output_tel
    puts "tel:#{@tel}"
  end

  def output_sign
    puts '👮より'
  end
end


plain = Plain.new(
  name: 'てすてす',
  address: 'ととと',
  tel: '090-zzzz-zzzz',
)
plain.issue

puts

resident = ResidentCard.new(
  name: 'かきくけ ここ',
  address: 'ちばなしけん',
  tel: '0120-yyy-yyy',
)
resident.issue

puts

driving = DrivingLicence.new(
  name: 'あいうえ おたろう',
  address: 'とうきょうとっきょ',
  tel: '0120-xxx-xxx',
)
driving.issue

puts

