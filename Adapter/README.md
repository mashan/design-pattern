Adapterクラス
============

概要
-----

互換性の無いクラスを仲介するインターフェイスをもたせ、動くようにする。
例えば、XLMしか出力できないデータストアとJSONしか解析できない分析ツールをつなぐなど。
既存クラスの書き換えが難しい(理解しきれない、影響が大きすぎるなど)場合に有効。

構成要素
-------

- Client : Targetのメソッドを呼び出す
- Target : 呼び出したい仕組み
- Adapter : 呼び出されたメソッドをレガシーなコンポーネントへとリダイレクトする
- Adaptee : 既存のクラス

AdapterはWrapperもしくは既存クラスのオーバーライドによって実装される。


