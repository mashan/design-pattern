require 'httparty'

# Adapter
class S3ObjectURI 
  def self.parse(path)
    # format: https://saito-test-repository.s3-ap-northeast-1.amazonaws.com/foo.txt
    protcol = 'https'
    bucket  = path.gsub('s3://').split['/'][0]
    key     =  path.gsub('s3://').split['/'][1..-1]

    host = [ bucket, @region, "amazon.com" ].join('.')
    URI::HTTP.build(
      scheme: protocol,
      host: host,
      path: key
    )
  end

  attr_reader :region, :path

  def initialize(region)
    @region = region
  end

  def path=(path)
    @path
  end

  def userinfo
    nil
  end

  def relative?
    nil
  end

  def query
    nil
  end

  def query=(query)
    nil
  end

  def scheme
    'https'
  end
end

# Target
class SimpleHTTPClient
  include HTTParty
  uri_adapter S3ObjectURI
end

s3_uri = S3ObjectURI.new('ap-northeast-1')
s3_uri.path = 's3://foo/baz.txt'

SimpleHTTPClient.get(s3_uri)
