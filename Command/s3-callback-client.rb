# Reciever
class S3Client
  def initialize(config)
    puts 'initializing S3Client'
  end

  def get_object(url)
    puts "Getting from #{url}"
    'ふー,ばず,ばー'
  end

  def put_object(url)
    puts "Upload to #{url}"
    '200'
  end
end

# Command
class Command
  def initialize(url)
    @url = url
  end

  def execute
    raise NotImplementedError
  end
end

# ConcreteCommand
class GetContent < Command
  def execute
    # stub HTTP get
    puts "Getting from #{@url}"
    'foo,baz,bar'
  end
end

class UploadContent < Command
  def execute
    # stub HTTP put
    puts "Upload to #{@url}"
    '200'
  end
end

class GetS3Object < Command
  def execute
    S3Client.new('dummy').get_object(@url)
  end
end

class UploadS3Object < Command
  def execute
    S3Client.new('dummy').put_object(@url)
  end
end

class NullCommand < Command
  def execute
    puts 'Nothing to do.'
  end
end

# Invoker
class Procedure
  def initialize(name)
    @name = name
  end

  def pre_task=(command)
    @pre_task = command
  end

  def task=(command)
    @task = command
  end

  def after_task=(command)
    @task = command
  end

  def run
    puts "Start #{@name}."
    @pre_task.execute
    @task.execute
  end
end

# Client

procedure = Procedure.new("Upload to s3 with a remote content.")
procedure.pre_task = GetContent.new('http://my-favarite-site.com/foo/baz.html')
procedure.task = UploadS3Object.new('s3://my-bucket/foo/baz/bar.html')
procedure.after_task = NullCommand.new('')
procedure.run

procedure = Procedure.new("Upload to remote url with my s3 object")
procedure.pre_task = GetS3Object.new('s3://my-bucket/foo/baz/bar.html')
procedure.task = UploadContent.new('http://my-favarite-site.com/foo/baz.html')
procedure.after_task = NullCommand.new('')
procedure.run

