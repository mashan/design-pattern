# ComcreteComponent
class SimpleClient
  attr_accessor :object

  def initialize(dest, object = nil)
    @dest = dest
    @object = object
  end

  def upload
    puts "Upload #{@object} to #{@dest}"
  end

  def download
    puts "Download object from #{@dest}"
    "😊"
  end
end

class ClientDecorator
  def initialize(client)
    @client = client
  end

  def upload
    @client.upload(@client.object)
  end

  def download
    @client.download
  end
end

class ExtendedUploadClient < ClientDecorator
  def initialize(client, download_url)
    super(client)
    @download_url = download_url
  end

  def upload
    puts "Download content from #{@download_url}"
    @client.object = "🤗"
    @client.upload
  end
end

class ExtendedDownloadClient < ClientDecorator
  def initialize(client, upload_url)
    super(client)
    @upload_url = upload_url
  end

  def download
    object = @client.download
    puts "Upload #{object} to #{@upload_url}"
  end
end

puts 'ExtendedUploadClient'
client = ExtendedUploadClient.new(SimpleClient.new('s3://my-bucket/sample.txt'), 'http://my-source.com/sample.txt')
client.upload

puts
puts 'ExtendedDownloadClient'
client = ExtendedDownloadClient.new(SimpleClient.new('s3://my-bucket/sample.txt'), 'http://my-dest.com/sample.txt')
client.download
