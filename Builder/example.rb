class ComputerBuilder
  attr_reader :computer

  def turbo(has_turbo_cpu = true)
    @computer.motherboard.cpu = TurboCPU.new
  end

  def memory_size=(size_in_mb)
    @computer.motherboard.memory_size = size_in_mb
  end
end

class DesktopBuilder < ComputerBuilder
  def initialize
    @computer = DesktopComputer.new
  end

  def display=(display)
    @display = display
  end

  def add_cd(writer = false)
    @computer.drives << Drive.new(:cd, 760, writer)
  end

  def add_dvd(writer = false)
    @computer.drives << Drive.new(:dvd, 40000, writer)
  end

  def add_hard_disk(writer = false)
    @computer.drives << Drive.new(:hard_disk, size_in_mb, writer)
  end
end

desktop_builder = LaptopBuilder.new
desktop_builder.display = "Eizo"
desktop_builder.computer
