class S3Client
  attr_accessor :authorization_settings, :region, :bucket, :path, :token, :pre_task, :post_task
end

class S3ClientAuthrizationSettings
  attr_accessor :path, :token
  def self.find(id)
    auth = self.new
    auth.path = 'foo'
    auth.token = 'baz'
    auth
  end

  def initialize
    @path
    @token 
  end
end


# Builder
class S3ClientBuilder
  attr_reader :s3_client

  def initialize
    @s3_client = S3Client.new
  end

  def find_authorization_settings(id)
    @s3_client.authorization_settings = S3ClientAuthrizationSettings.find(id)
  end

  def region=(region)
    @s3_client.region = region
  end

  def bucket=(bucket)
    @s3_client.bucket = bucket
  end

  def path=(path)
    @path = path
  end

  def token=(token)
    @token = token
  end

  def build
    if @s3_client.authorization_settings
      @s3_client.path = @s3_client.authorization_settings.path
      @s3_client.token = @s3_client.authorization_settings.token
    end

    if !@s3_client.path 
      @s3_client.path = @path
    end

    if !@s3_client.token 
      @s3_client.token = @token
    end

    if @s3_client.path.nil? || @s3_client.token.nil?
      raise 'You must configure authorization_settings or path or token'
    end

    @s3_client
  end
end

builder = S3ClientBuilder.new
builder.find_authorization_settings(1)
builder.region = 'ap-northeast-1'
builder.bucket = 'mybucket'
client = builder.build

p client

