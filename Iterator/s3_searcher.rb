class S3Object
  attr_reader :key, :content, :uploaded_at

  def initialize(key, content, uploaded_at)
    @key = key
    @content = content
    @uploaded_at = uploaded_at
  end
end

class Bucket
  attr_reader :objects

  def initialize
    @objects = []
  end

  def add(object)
    @objects << object
  end

  def get_by_id(index)
    @objects[index]
  end

  def get_by_key(key)
    @objects.select{|object| object.key == key}.first
  end

  def key_iterator
    ObjectKeyIterator.new(self)
  end
end

class ObjectIteratorBase 
  def initialize(bucket)
    @objects = bucket.objects
    @index = 0
  end

  def each(&block)
    @objects.each(&block)
  end

  def has_next?
    @index < @objects.size
  end

  def has_previous?
    @index > 0
  end

  def done?
    @index == @objects.length - 1
  end

  def next
    object = nil

    if has_next?
      object = @objects[@index]
    end

    @index += 1
    object
  end

  def current
    @objects[@index]
  end

  def previous
    object = nil

    if has_previous?
      object = @objects[@index]
    end

    @index -= 1
    object
  end
end

class ObjectKeyIterator < ObjectIteratorBase
  def initialize(bucket)
    super
    @objects = bucket.objects.sort{|a,b| a.key <=> b.key}
  end
end

object01 = S3Object.new('foo', 'あああああ', '2019-08-01')
object02 = S3Object.new('baz', 'いいいいい', '2019-08-02')
object03 = S3Object.new('bar', 'ううううう', '2019-08-03')

bucket = Bucket.new
bucket.add(object01)
bucket.add(object02)
bucket.add(object03)

bucket.key_iterator.each do |object|
  p object
end


