class S3Resource
  AUTHORIZED_TOKENS = %w| foo baz bar |

  def initialize(bucket, key, token)
    @bucket = bucket
    @key = key
    @token = token
  end

  def get
    check_access
    puts 'Get 🤗'
  end

  def put
    check_access
    puts 'Put 🤣'
  end

  def list
    check_access
    puts '🤗　🤣　🤩'
  end

  def check_access
    if ! AUTHORIZED_TOKENS.include?(@token)
      raise "Your toke (#{@token}) is not allowed!"
    end
  end
end

class S3Proxy
  AUTHORIZED_USERS = %w| aizawa takeuchi |

  def initialize(s3_resource, user_name)
    @s3_resource = s3_resource
    @user_name = user_name
  end

  def get
    check_access
    @s3_resource.get
  end

  def put
    check_access
    @s3_resource.put
  end

  def list
    check_access
    @s3_resource.list
  end

  def check_access
    if ! AUTHORIZED_USERS.include?(@user_name)
      raise "#{@user_name} is not allowed!"
    end
  end
end

s3_with_dummy_token = S3Resource.new('saito-example', 'index.html', 'dummy')
proxy_with_dummy_token = S3Proxy.new(s3_with_dummy_token, 'aizawa')

begin
  proxy_with_dummy_token.get
rescue => e
  puts e
end

s3 = S3Resource.new('saito-example', 'index.html', 'foo')
proxy_with_not_authorized_user = S3Proxy.new(s3, 'saito')

begin
  proxy_with_not_authorized_user.get
rescue => e
  puts e
end

proxy_with_aizawa = S3Proxy.new(s3, 'aizawa')
proxy_with_aizawa.get

proxy_with_takeuchi = S3Proxy.new(s3, 'takeuchi')
proxy_with_takeuchi.put

proxy_with_takeuchi.list
