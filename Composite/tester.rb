###############
# Component
###############

class Test
  attr_reader :name
  attr_accessor :parent

  def initialize(name)
    @name = name
    @parent = nil
  end

  def execute
  end
end

class CompositeTest < Test
  def initialize(name)
    super(name)
    @sub_tests = []
  end

  def add_test(test)
    @sub_tests << test
    test.parent = self
  end

  def remove_test(test)
    @sub_tests.delete(test)
    test.parent = nil
  end

  def execute
    puts "Run #{self.name}"
    @sub_tests.each do |test|
      test.execute
    end
  end
end

###############
# Leaf
###############

# Leaf of Load Test
# -----------------

class CpuLoadTest < Test
  def initialize
    super('CPU Load Test')
  end

  def execute
    puts "Run #{self.name}"
    puts 'Running `while do:; expr 1 + 1; done'
  end
end

class IoLoadTest < Test
  def initialize
    super('IO Load Test')
  end

  def execute
    puts "Run #{self.name}"
    puts 'Running `dd if=/dev/zero of=/tmp/write.tmp ibs=1M obs=1M count=1024`'
  end
end

class NetworkLoadTest < Test
  def initialize
    super('Network Load Test')
  end

  def execute
    puts "Run #{self.name}"
    puts 'Running `iperf -c 127.0.0.1`'
  end
end

# Leaf of Application Test
# -----------------

class IntegrationApplicationTest < Test
  def initialize
    super('Integration Test')
  end

  def execute
    puts "Run #{self.name}"
    puts '........F.......'
  end
end


class UnitApplicationTest < Test
  def initialize
    super('Unit Test')
  end

  def execute
    puts "Run #{self.name}"
    puts '...F.......'
  end
end


###############
# Composite
###############

# Composite of Load Test
# -----------------

class LoadTest < CompositeTest
  def initialize
    super('Load Test')
    add_test(CpuLoadTest.new)
    add_test(IoLoadTest.new)
    add_test(NetworkLoadTest.new)
  end
end

class ApplicationTest < CompositeTest
  def initialize
    super('Application Test')
    add_test(IntegrationApplicationTest.new)
    add_test(UnitApplicationTest.new)
  end
end

class Tester < CompositeTest
  def initialize
    super('Tester')
    add_test(LoadTest.new)
    add_test(ApplicationTest.new)
  end
end

Tester.new.execute
