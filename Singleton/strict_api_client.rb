require 'singleton'
require 'net/https'

class StrictApiClient
  include Singleton
  attr_reader :client

  def initialize
    @client = Net::HTTP.new("www.yahoo.co.jp", 443)
    @client.use_ssl = true
    @client.start
  end

  def close
    @client.finish
  end
end

strict_api_client = StrictApiClient.instance
client = strict_api_client.client

2.times do
  req = Net::HTTP::Get.new("/")
  req["Connection"] = "Keep-Alive"
  res = client.request(req)
  p res
  res.each do |k, v|
    puts "#{k}: #{v}"
  end
end

strict_api_client.close
