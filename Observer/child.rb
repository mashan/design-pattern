require 'observer'

class GrowthRecord
  include Observable

  def initialize(child)
    @child = child
    @last_record = nil
  end

  def record
    30.times do
      @last_record = @child.dup

      @child.grow
      changed(!(@child == @last_record))

      notify_observers(Time.now, @child)

      sleep 1
    end
  end
end

class Child
  attr_reader :name, :height, :weight
  def initialize(name:, height:, weight:)
    @name     = name
    @height   = height
    @weight   = weight
  end

  def grow
    @height += rand(20) if @height <= 180
    @weight += rand(10) if @weight <= 100
  end

  def ==(other)
    return @name == other.name && @height == other.height && @weight == other.weight
  end
end

class Parent
  def initialize(growth_record)
    growth_record.add_observer(self)
  end

  def update(time, child)
    puts "まぁ、#{child.name}が大きくなったわ！身長は#{child.height}cm、体重は#{child.weight}kgよ！"
  end
end

foo = Child.new(name: 'foo子', height: 50, weight: 1)
growth_record = GrowthRecord.new(foo)
Parent.new(growth_record)

growth_record.record

