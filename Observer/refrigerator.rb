require 'observer'

class Stock
  include Observable

  attr_reader :name, :quantity

  def initialize(name:, quantity:, observers: [])
    @name = name
    @quantity = quantity
    observers.each do |observer|
      add_observer(observer.new)
    end
  end

  def put(quantity)
    @quantity += quantity
    changed
    notify_observers(self)
  end

  def take(quantity)
    @quantity -= quantity
    changed
    notify_observers(self)
  end
end

class GoogleHome
  def update(stock)
    puts "#{stock.name}の在庫が#{stock.quantity}になりました。Google検索します。"
  end
end

class Alexa
  def update(stock)
    puts "#{stock.name}の在庫が#{stock.quantity}になりました。Amazonで購入しておきました。"
  end
end


beer_stock = Stock.new(name: '🍺', quantity: 6, observers: [Alexa, GoogleHome])
beer_stock.take(2)
beer_stock.take(2)
beer_stock.take(2)
