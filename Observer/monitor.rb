require 'observer'

class Monitor
  include Observable

  def initialize(host)
    @host = host
  end

  def run
    last_load_average = 0.0

    loop do
      load_average = @host.load_average

      if load_average != last_load_average
        changed
        last_load_average = load_average
        notify_observers(Time.now, @host, load_average)
      end

      sleep 1
    end
  end
end

class Host
  def initialize(host)
    @host = host
  end

  def load_average
    rand(50) * 0.1
    #`w | egrep -o 'load average: [0-9]+\.[0-9]+' | cut -d' ' -f 3`.chomp.to_i
  end
end

class Threshold
  def initialize(monitor, limit)
    @limit = limit
    monitor.add_observer(self)

    @exceed_threshold = false
  end

  def update(time, host, load_average)
    case
    when !exceed_threshold? && load_average >= @limit
      puts "[#{time}] #{host}'s Load Average is High! #{load_average}"
      exceed_threshold
    when exceed_threshold? && load_average < @limit
      puts "[#{time}] #{host}'s Load Average is OK! #{load_average}"
      below_threshold
    end
  end

  private

  def exceed_threshold
    @exceed_threshold_flag = true
  end

  def below_threshold
    @exceed_threshold_flag = false
  end

  def exceed_threshold?
    @exceed_threshold_flag
  end
end

myhost = Host.new('myhost')
monitor = Monitor.new(myhost)
Threshold.new(monitor, 3)
monitor.run
