class Creator
  def initialize(settings)
    @settings = settings
  end

  def factory_method
    raise NotImplementedError
  end

  def run
    product = factory_method

    result = product.operation

    result
  end
end

class UploaderWithPreTaskCreator < Creator
  def factory_method
    UploaderWithPreTask.new(@settings)
  end
end

class DownloaderWithPostTaskCreator < Creator
  def factory_method
    DownloaderWithPostTask.new(@settings)
  end
end

# Base of Product 
class ObjectClient
  attr_reader :settings
  def initialize(settings)
    @settings  = settings
  end

  def operation
    raise NotImplementedError
  end

  def upload
    puts 'Upload to s3://myproducts/foo.txt'
  end

  def download
    puts 'Download from s3://myproducts/foo.txt'
  end

  private

  def pre_task
  end
  
  def post_task
  end
end

class UploaderWithPreTask < ObjectClient
  def operation
    pre_task
    upload
  end

  private

  def pre_task
    puts 'Donwload a content from http://product.com'
  end
end

class DownloaderWithPostTask < ObjectClient
  def operation
    upload
    post_task
  end

  private
  def post_task
    puts 'Upload a content from http://product.com'
  end
end

def client(creator)
  puts "Client: #{creator.run}"
end

settings = {
  id: 11111,
  credential: 'oiufkj'
}

client(UploaderWithPreTaskCreator.new(settings))
client(DownloaderWithPostTaskCreator.new(settings))
