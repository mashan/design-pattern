Factory
=======

インスタンス作成のインターフェイスを定義したクラスを作り、そのサブクラスによってインスタンス作成を行う。

Template Methodパターンのインスタンス生成バージョン。


- Creator : ConcreteCreatorの基底クラス
- ConcreteCreator : 実際にオブジェクト生成を行うクラス
- Product : ConcreteCreatorによって生成されるオブジェクト
