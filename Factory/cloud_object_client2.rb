class Creator
  def initialize(setting)
    @tasks = []

    @tasks << new_pre_task(setting)
    @tasks << new_main_task(setting)
    @tasks << new_post_task(setting)

    @tasks.compact!
  end

  def run
    @tasks.each do |task|
      task.run
    end
  end
end

class UploaderWithPreTaskCreator < Creator
  def new_pre_task(setting)
    PreTask.new(setting)
  end

  def new_main_task(setting)
    MainTaskUpload.new(setting)
  end

  def new_post_task(setting)
  end
end

class DownloaderWithPostTaskCreator < Creator
  def new_pre_task(setting)
  end

  def new_main_task(setting)
    MainTaskDownload.new(setting)
  end

  def new_post_task(setting)
    PostTask.new(setting)
  end
end

# Base of Product 
class ObjectClient
  def initialize(setting)
  end

  def run
    raise NotImplementedError
  end

  private

  def upload(url)
    puts "Upload to #{url}"
  end

  def download(url)
    puts "Download from #{url}"
  end
end

class PreTask < ObjectClient
  def run
    download('https://product.com/foo.csv')
  end
end

class MainTaskUpload < ObjectClient
  def run
    upload('s3://myproducts/foo.csv')
  end
end

class MainTaskDownload < ObjectClient
  def run
    download('s3://myproducts/foo.csv')
  end
end

class PostTask < ObjectClient
  def run
    upload('https://uploader.product.com/foo.csv')
  end
end


def client(creator)
  puts "Client: call #{creator}"
  creator.run
end

setting = {
  id: 11111,
  credential: 'oiufkj'
}

client(UploaderWithPreTaskCreator.new(setting))
client(DownloaderWithPostTaskCreator.new(setting))
