class Certificate
  attr_reader :name, :address, :tel, :photo
  attr_accessor :strategy

  def initialize(name:, address:, tel:, photo:, strategy:)
    @name     = name
    @address  = address
    @tel      = tel
    @photo    = photo
    @strategy = strategy
  end

  def issue
    @strategy.issue(self)
  end
end

class ResidentCardStrategy
  def issue(obj)
    puts <<-EOS
--------------------------|
| 住民票                  |
--------------------------|
| 氏名 | #{obj.name}    |
| 住所 | #{obj.address} |
| 電話 | #{obj.tel}     |
--------------------------
    EOS
  end
end

class DrivingLicenceStrategy
  def issue(obj)
    puts <<-EOS
--------------------------------------------
| 氏名 : #{obj.name} | tel : #{obj.tel}   |
--------------------------------------------|
| 住所 : #{obj.address} | #{obj.photo}    |
--------------------------------------------
    EOS
  end
end

cert = Certificate.new(
  name: 'あいうえお太郎',
  address: 'でっかいどう',
  tel:  '0120-xxx-xxx',
  photo: '👼',
  strategy: ResidentCardStrategy.new
)
cert.issue

puts

cert.strategy = DrivingLicenceStrategy.new
cert.issue
