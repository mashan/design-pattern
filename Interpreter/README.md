Interpreterパターン
==================

専用の言語を作り、その言語で得られた手順にもとづいて処理を実行していくパターン

- AbstractExpression : 共通のインターフェイスを定義
- TerminalExpression : 終端を表現するクラス
- NotTerminalExpression : 非終端を表現するクラス
- Context : 構文の解釈を手助けするもの
