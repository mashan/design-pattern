class S3Client
  def initialize(credentials: , destination:,  source:)
    @credentials = credentials
    @destination = destination
    @source      = source
  end

  def put
    puts "Uploaded #{@source} to #{@destination}.(by #{@credentials})"
  end
end

class User
  def initialize(user)
    @user = user
  end

  def execute
    @user
  end
end

class Source
  def initialize(source)
    @source = source
  end

  def execute
    @source
  end
end

class Destination
  def initialize(dest)
    @dest = dest
  end

  def execute
    @dest
  end
end

class Put
  attr_accessor :user, :dest, :src
  
  def execute
    S3Client.new( credentials: user.execute, destination: dest.execute, source: src.execute).put
  end
end


def parser(input)
  stack = input.split 

  credential = User.new(stack.shift)

  case stack.shift
  when "put"
    action = Put.new
    source = Source.new(stack.shift)
    destination = Destination.new(stack.shift)
  when "get"
    #action = Get.new
    #destination = Destination.new(stack.shift)
    #source = Source.new(stack.shift)
  end

  action.user = credential
  action.dest = destination
  action.src  = source

  action.execute
end

parser('Taro put ./file.txt s3://aaaa/bbb')
