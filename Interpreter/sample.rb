require 'find'

class Expression
  def |(other)
    Or.new(self, other)
  end

  def &(other)
    And.new(self, other)
  end
end

# AbstractExpression
class All < Expression
  def evaluate(dir)
    results = []
    Find.find(dir) do |f|
      next unless Find.file?(f)
      results << f
    end
  end
end

# TerminalExpression
class FileName < Expression
  def initialize(pattern)
    @pattern = pattern
  end

  def evaluate(dir)
    results = []

    Find.find(dir) do |f|
      next unless File.file?(f)
      name = File.basename(f)
      results << f if File.fnmatch(@pattern, name)
    end

    results
  end
end

# TerminalExpression
class Bigger < Expression
  def initialize(size)
    @size = size
  end

  def evaluate(dir)
    results = []

    Find.find(dir) do |f|
      next unless File.file?(f)
      results << f if File.size(f) > @size
    end

    results
  end
end

# TerminalExpression
class Writable < Expression
  def evaluate(dir)
    results = []

    Find.find(dir) do |f|
      next unless File.file?(f)
      results << f if File.writable?(f)
    end

    results
  end
end

# NonTerminalExpression
class Not < Expression
  def initialize(expression)
    @expression = expression
  end

  def evaluate(dir)
    All.new.evaluate(dir) - @expression.evaluate(dir)
  end
end

# NonterminalExpression
class Or < Expression
  def initialize(expression1, expression2)
    @expression1 = expression1
    @expression2 = expression2
  end

  def evaluate(dir)
    result1 = @expression1.evaluate(dir)
    result2 = @expression2.evaluate(dir)
    (result1 + result2).sort.uniq
  end
end

# NonterminalExpression
class And < Expression
  def initialize(expression1, expression2)
    @expression1 = expression1
    @expression2 = expression2
  end

  def evaluate(dir)
    result1 = @expression1.evaluate(dir)
    result2 = @expression2.evaluate(dir)
    (result1 & result2)
  end
end

#complex_expression1 = And.new(FileName.new('*.txt'), FileName.new('a*'))
#puts complex_expression1.evaluate('/Users/sai/Work')
#
#complex_expression2 = Bigger.new(1024)
#puts complex_expression2.evaluate('/Users/sai/Work/Gitlab')
#
complex_expression3 = FileName.new('*.txt') | Bigger.new(1024 * 1024)
puts complex_expression3.evaluate('/Users/sai/Work/Gitlab')
